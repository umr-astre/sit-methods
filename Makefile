## Remote computation of the log-likelihood on a grid of values
##
## This requires passwordless access to baicorre server.

## Files to copy on the remote server in order to carry out the computation
## The computation script is likelihood_sit_scenario.R
## It requires some functions, and the objects sit_scenario and sim_trap_agegroup
FILES=src/functions.R src/likelihood_sit_scenario.R _targets/objects/sit_scenario _targets/objects/sim_trap_agegroup

help:
	@echo 'Run `make compute` to launch the computation remotely'
	@echo 'Run `make check` to check the number of remotely running processes. More than 2 indicate ongoing processing.'
	@echo 'Run `make retrieve` to retrieve the results into `reports/ll_sit_scenario.rds`'

compute:
	# Copy files over
	rsync -azv --progress $(FILES) baicorre:sit-computing/
	# Run script
	nohup ssh baicorre "cd sit-computing; Rscript likelihood_sit_scenario.R" &

check:
	## How many processes are currently running remotely ?
	## Any more than 2 indicate that it is still running
	ssh baicorre "ps -ef | grep facu | grep parallelly | wc -l"

retrieve:
	rsync -azv --progress baicorre:sit-computing/ll_sit_scenario.rds reports/
