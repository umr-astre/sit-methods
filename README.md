# SIT Methods

Facundo Muñoz \
September 05, 2022

Modelling SIT trials.

- [Working document (HTML)](https://umr-astre.pages.mia.inra.fr/sit-methods/sit-methods.html)   
  [[PDF](https://umr-astre.pages.mia.inra.fr/sit-methods/sit-methods.pdf)]

- [Calculations (HTML)](https://umr-astre.pages.mia.inra.fr/sit-methods/calculations.html)
  [[PDF](https://umr-astre.pages.mia.inra.fr/sit-methods/calculations.pdf)]
  
- [Simulation study (HTML)](https://umr-astre.pages.mia.inra.fr/sit-methods/simulation_study.html)

- [Simulation animation (GIF)](https://umr-astre.pages.mia.inra.fr/sit-methods/sim_animation.gif)

![Simulation animation (GIF)](https://umr-astre.pages.mia.inra.fr/sit-methods/sim_animation.gif)
