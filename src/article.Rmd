---
title: "Survival and dispersion model using capture time for SIT"
author: "CAPEL Alexandre & MUÑOZ Facundo"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
output:
  pdf_document:
    template: null
    toc: false
documentclass: cirad
include-before:
- '\newcommand{\p}{\mathbb{P}}'
- '\renewcommand{\d}{\mathop{}\!{\mathrm{d}}}'
- '\newcommand{\norm}[1]{\lVert #1 \rVert}'
# citation_package:
#   natbib:
#     natbiboptions: "numbers, sort&compress"
# csl: plos-one.csl	
abstract: "The goal is the modelization of mosquitoes displacement in the context of the Sterile Inset Technique. In that way, we used some hypotheses known in the litterature about the spreading of this population and make hypothese for the rate of capture for each trap in the experiment area. However, each trap and death are disjoint outcomes that we need to take in account in our observation model of the capture times : competing risk theory is needed. Finally, we will suggest a likelihood for the problem and discuss about the identifiability of the parameters using the latter for a potential estimation of them."
bibliography: sit-methods.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE,
  dev = ifelse(is_latex_output(), "cairo_pdf", "png"),
  dpi = 300
)

theme_set(theme_ipsum(grid = "Y"))
```


```{r, echo=FALSE}
library(ggplot2)
library(ggh4x)
library(tidyverse)
library(dplyr)
library(hrbrthemes)
library(targets)
source("./src/functions.R")
tar_load(sit_scenario)

params <- sit_scenario@parms 
beta <- (log(params$pc0) - log(params$pc5)) / 5**2
param_surv <- -log(params$pds)

simpars <- simulation_parameters(sit_scenario)

```




# Introduction


The Sterile Insect Technique (SIT), used to control insect populations, consists of releasing males sterilized by ionizing radiations. Wild females that mate with these males can no longer produce viable offspring, which may drives the population decline. 

This method is often use to control the spreading of diseases, using on mosquitoes population (recently @ernawan_current_2019 for the dengue virus). Then, we usually try to capture the released individuals with several traps and try to estimate the effectiveness of the method. There exist a lot of models whose each can be used in order to compute some interesting quantities. 

For example, we can estimate the spread's velocity of the population using the Flight Range (FR) or the Mean Distance Traveled (MDT) or the methodology from (@muir1998aedes) to compute the daily death rate of the population. However, all of these method works independently each other that is not really the case : the speed diffusion and the death rate of a population are surely bounded by the specie of the individuals. 

More, there is not yet probabilistic model to produce simulation of the experiment in theorical context. That is why we choose to present you in this paper a first proposition of modelization for the capture times for each trap which take in account these survival and dispersion parameters, as well as some other parameters (coming from the traps).


# Initial model 

In this section, we will present the firsts hypotheses for the model, especially about the hazard function of the capture time. 

Let $T$ a random variable, taking values in $\mathbb R_+$, which refers to the capture time of an individual.

## Dispersion of the individual

First, we would like to modeling the movement of a mosquito. We will assume with no stimuli, mosquitoes disperse following a Brownian motion (@dumontSpatiotemporalModelingMosquito2011). In that, we mean that they fly around randomly, independently from each other , changing directions continuously, with no preferential direction of flow. 

Lets note $\rho_t$ the density of the population at time $t$. Mathematically, the above paragraph means that this function satisfies the diffusion equation, that is to say : 

$$
\frac{\partial \rho_t(x)}{\partial t} = D \nabla^2 \rho_t(x)
$$
\noindent for all $x \in \mathbb{R}^2$ ($\nabla^2 = \nabla \cdot \nabla$ is the Laplace operator).

In this equation, $D$ is the diffusion constant and represents the velocity of the individual, in terms of population the rate of spread of the individuals.


Thus, if the population is released from the origin of the spatial coordinates at the initial time $t=0$, one can show that the position $X(t)$ of an individual is a Gaussian vector such that : 

\begin{equation}
X(t) \sim \mathcal{N}(0, (2Dt)I_2)
\label{diffusion}
\end{equation}

\noindent where $I_2$ is the identity matrix of dimension 2.

## Probability of capture

First, let's assume that $X(t) = (0,0)$ for all $t>0$ and that the trap location is $x_0 \in \mathbb{R}^2$.

We assume that, if an individual stays as the same location $x_0$, the hazard function of the capture time $T$ is constant in time and decrease exponentially with the square of the distance $r = ||x_0|| >0$ between the trap and the position of the individual : 

\begin{equation}
h_T(t;r) = \lambda_r = \alpha e^{-\beta r^2}, \quad \alpha, \beta >0.
\label{hazard_unmoving}
\end{equation}

But here, the hazard function is only for unmoving individual. Merging this hypotheses with the diffusion displacement, we get the following result. 

\noindent \textbf{Proposition.} Let $T$ the capture time where the hazard rate of unmoving individuals is given by the equation (\ref{hazard_unmoving}). If the location of the individual at time $t$ is given by (\ref{diffusion}), we have : 
$$
h_T(t) = \frac{\alpha}{1+4D\beta t } e^{-\frac{\beta ||x_0||^2}{1+4D\beta t}}
$$

\noindent for all $t \ge 0$.

For example, these are the shape of the hazard function for different location when $D=150$, $\alpha = 0.8$ and $\beta= 0.2$.

```{r hazard-function}
#| fig-cap: Hazard functions of the capture times for traps at increasing distances from the release location with the dispersion and capture parameters used in the simulation.
#| fig.align: center
#| fig.height: 4
#| fig.width: 6


probe_traps <- tribble(
  ~x, ~y,
   0,  7,
   0, 14,
   0, 28,
   0, 56,
) |> 
  mutate(
    dist = round(sqrt(x**2 + y**2), 1),
    trap = map2_chr(x, y, ~paste0('(', .x, ', ', .y, ')'))
  )

probe_hazards <- probe_traps |> 
  expand_grid(
    ## Evaluation times (more concentrated near 0, where variations are larger)
    t = exp(seq(-9, 1.6, length = 51))
  ) |> 
  ## Evaluate the hazard function for each trap, at each time, using the
  ## simulated parameters.
  rowwise() |> 
  mutate(
    h = map_dbl(
      t,
      ~ hazard_one(
        ., D = simpars$D, alpha = simpars$a, beta = simpars$b, x0 = c(x, y)
      )
    )
  ) |> 
  ungroup()

ggplot(probe_hazards) +
  aes(t, h, colour = factor(dist)) +
  geom_line() +
  geom_text(
    data = ~ mutate(., hm = max(h), .by = trap) |> 
      filter(t == max(t)),
    aes(y = hm, label = paste(dist, "m")),
    hjust = 0,
    vjust = 1
  ) +
  labs(
    x = "Time (days)",
    y = "Hazard rate (captures/day)",
    colour = "Distance (m)"
  ) +
  coord_cartesian(clip = 'off') +
  guides(
    colour = "none"
  )

```

In the figure above, we observe the curves are different according to the location in the area experiment. Indeed, we get a strong and concentrated mode in the beginning for the closest traps whereas we see a curve more diffuse for those who are further. An intuitive conclusion : the individuals can not be captured before they arrive around the trap.


# Likelihood of a several trap setting model 

In the previous section, we saw the hazard rate of the capture time for one individual without considering the death, or the risk to be trapped in another trap. We have to introduce new paradigm using the competing risk. 

## Multitrap model

Competing risk are used when the observed times come from different outcomes. In that sense, the occurrence of any outcome which is not the standard outcome leads to a situation where the time of this outcome is unobservable for the individual (@morita_introduction_2021).

Now, let $I$ the number of trap during the experiment, and let assume that the death hazard rate is constant equal to $\lambda >0$.

The possible outcomes which are going to compete are the capture in a trap $i \in \{1,\dots,I\}$, or the death. Thus, we will introduce a variable $C$, called fate variable, which will indicate what kind of outcome the time is about ($i$ for the trap $i$, 0 if dead).


In @austin_introduction_2016, they explain that we can use the cause specific hazard rate defined by : 

$$
h^{cs}_i(t) = \lim_{dt \rightarrow0} \frac{\p(t\le T\le t+dt, C=i, |T >t)}{dt}
$$
\noindent for $i = 0, 1, \dots, I$.

In our case we can give an expression of this function for all $i$. Indeed, if we note $x_i$ the position of the trap $i = 1,\dots , I$, we have : 

\begin{equation}
	h_i^{cs}(t) = \begin{cases}
	\quad \quad	\quad \lambda  & \text{if } i = 0 \\
		\frac{\alpha}{1+4D\beta t}e^{-\frac{\beta||x_i||^2}{1+4D\beta t}}  &\text{else}.
	\end{cases}
\end{equation}

Now, with these functions, we can get the global hazard rate by summing the later. We can deduce : 

\begin{equation}
		h(t) = \sum_{i=0}^I h_i^{cs}(t) \quad \text{and} \quad
		S(t) = \exp\left(-\int_0^t h(u) du \right).
\label{hazard_survival}
\end{equation}

\noindent where $S$ is the global survival function of the capture time $T$.

To conclude, to have the likelihood of the model we need to have the joint distribution of the pair $(T,C)$. But using the partial cumulative density function $F_i(t) = \p(T\le t, C=i)$, we are able to compute the subdensity $f_i$ of the couple $(T,C)$ defined as the derivative in $t$ for $F_i$. In particular, these function allows us to write the relation : 

\begin{equation}
	\p(t_l \le T \le t_r, C=i) = \int_{t_l}^{t_r} f_i(t)dt = \int_{t_l}^{t_r} h^{cs}_i(t) S(t)dt.
	\label{cum_subdensity}
\end{equation}

## Observation model

The relation of the equation (\ref{cum_subdensity}) will be useful for the construction of our likelihood applied to the observation model. Let $\theta = (\lambda,D, \alpha, \beta) \in (\mathbb{R}_+)^4$, the parameters, and $f_i^\theta$ and $S^\theta$ the subdensity and survival function according to these parameters respectively.

To remember, we observe the numbers $Y_{ij}$ of captured individuals in traps $i = 1, \ldots, I$ at survey $j = 1, \ldots, J_i$. The survey periods for trap $i$ span from day
$t_{ij}^0$ to $t_{ij}^1$.

Assuming that $N$ individuals were released at day $t = 0$, and that each of them has a cumulative capture probability on trap $i$ at survey $j$ described by (\ref{cum_subdensity}), the number of captures $Y_{ij}$ follow a Binomial distribution with parameters
$N$ and $p^\theta_{ij} = \int_{t_{ij}^0}^{t_{ij}^1} f^\theta_i(t)dt$.

However, each of the $N$ individuals can either end up in only one of the surveys $i, j$ or not being captured in the course of its life with probability $p^\theta_0 = \lambda \int_{\mathbb{R}_+} S^\theta(t)dt$ (using equation (\ref{cum_subdensity})) . Thus, the outcome vector $(N - \sum_{ij}{Y_{ij}}, Y_{ij})$ follows a Multinomial distribution with $N$ trials, $K = 1 + \sum_{i = 1}^I J_i$ mutually exclusive events and probabilities $p^\theta_k$ where,
\[
  p^\theta_0 + \sum_{i = 1}^I \sum_{j = 1}^{J_i} p^\theta_{ij} = 1
\]

\noindent Thus, we can deduce from this observation the expression of the likelihood : 

\begin{equation}
\mathcal{L}(\theta) = P(Y_{ij} \mid \theta, N) = \prod_{i = 1}^I \prod_{j = 1}^{J_i} (p^{\theta}_{ij})^{Y_{ij}} \times (p^{\theta}_0)^{(N - \sum_{ij}{Y_{ij}})}
\end{equation}

And so, the following log-likelihood : 

\begin{equation}
l(\theta) = \sum_{i = 1}^I \sum_{j = 1}^{J_i} \big[ Y_{ij} \log(p^{\theta}_{ij}) \big] + (N - \sum{Y_{ij}}) \log(p^{\theta}_0)
\label{llh}
\end{equation}

The expression (\ref{llh}) will be used in the next section.

# Simulation

Now, we will use our simulator and see if our model fits well with the results.

## Set up

For the location of the traps, we used the design of a real experiment from Havana, Cuba (Gato, R., and E. al. 2022. Mark-Release-Recapture of irradiated sterile male Aedes aegypti in Cuba, in preparation of a pilot control trial. Insects in press. Gato, R., Z. Menéndez, E. Prieto, R. Argilés, M. Rodríguez, W. Baldoquín, Y. Hernández, D. Pérez, J. Anaya, I. Fuentes, C. Lorenzo, K. González, Y. Campo, and J. Bouyer. 2021. Sterile Insect Technique: Successful Suppression of an Aedes aegypti Field Population in Cuba. Insects 12: 469. Check). 

Then, we chose to drop $N = 50000$ individuals at the origin of the experiment area and : 

- a dispersion parameters $D = 150$. 

- a probability of capture for an individual located at the trap of $0.8$

- a probability of capture for an individual located at 5 meters from the trap of $0.1$ (useful to get $\beta$ parameters).

- a probability of daily survival of 0.8

\textbf{Note.} The points 2 and 3 are interpretable and used to set the trap parameters $\alpha$ and $\beta$. The $\alpha$ parameter can be seen as the capture rate and the $\beta$ parameter as the capture extent of the trap.

## Results

Let show the results of the simulation and how well it fits at our model.

\textbf{Histogram and fitting of the conditionnal density}

First, we can see the real capture time (that we do not observe in the model presented in the previous section) conditionally to the capture at a trap. In our case, we observe individuals in the nine closest traps from the released point, these are the histogram for each one :   

```{r histogram, echo=FALSE, fig.align='center', fig.height=6, fig.width=9}
#| fig-cap: Histograms of the capture time by trap and their respecitive conditionnal density
# Construction capture times
capture <- data.frame(
  id = factor(inverse.rle(
          structure(
            list(
              lengths = trap_counts(sit_scenario)$n,
              values = trap_counts(sit_scenario)$id
            ),
            class = 'rle'
          )
        )),
  t = inverse.rle(
          structure(
            list(
              lengths = trap_counts(sit_scenario)$n,
              values = trap_counts(sit_scenario)$t
            ),
            class = 'rle'
          )
        )
)

# Model parameters
loc <- as.matrix(params$traps.loc[,c(2,3)])
pars <- list(pds = params$pds,
             diffusivity = params$diffusivity,
             alpha = params$pc0, 
             beta = beta)

# Survival end specific hazard functions
S <- overall_survival(loc, pars)
h <- expected_specific_hazard_functions(loc, pars)

# Data for the density
dat <- expand.grid(t = seq(0, 20, 0.1), id = factor(seq.int(21))) |> 
  as_tibble() |>
  group_by(id) |>
  mutate(z = h(.data$t)[(1:201) + as.numeric(id)*201,]$h * S(.data$t))

dat_final <- dat |> 
  group_by(id) |>
  mutate(f = z / sum(0.1 * z)) |>
  filter(id %in% c(3, 4, 5, 6, 7, 8, 13, 18, 19))

# Graph
ggplot()+ 
    geom_histogram(data = capture[2:1261, ], aes(x = t, y = after_stat(density)), 
                   alpha = 0.6, fill = "darkblue",
                   bins = 15)+ facet_wrap(~id) + 
    geom_line(data = dat_final, aes(x = t, y = f), color='darkred')+
    labs(x = "t",
         y = "Density")+
    theme(axis.title = element_text(family = "Arial"))
```


We see here similar observations that we have done in the section 2 about the hazard function of the capture time in one trap setting.


```{r KM.test, echo = FALSE, eval = FALSE}
# Computation of the cumulative distribution function

H <- Vectorize(\(.) sum(h(.)$h[2:22]))


f_stat <- \(.) H(.)*S(.)
cte <- sum(f_stat(seq(0, 20, 0.1)) * 0.1)

simu <- sort(capture$t)
n <- length(simu)
res <- rep(0,n)
res[1] <- sum(f_stat(seq(0, simu[1],0.001)))*0.001
for(i in 2:n){
  if(simu[i - 1] == simu[i]){
    res[i] <- res[i - 1]
  }else{
    res[i] <- res[i - 1] + sum(f_stat(seq(simu[i - 1], simu[i],0.001)))*0.001
  }
}

# Statistic test construction 
a <- seq(0, n - 1,1) / n
b <- seq(1, n, 1) / n
K <- sqrt(n) * max(c(max(abs(res/cte - a)), max(abs(res / cte - b))))

# < 0.05 the 0.2 critical value : so we can't reject H_0

```

Of course, we can compute the Kolmogorov-Smirnov statistic for our general model and test the fitting of it on the simulated data. We get the value $0.0142$ which is under the critical value ($5 \%$) equalt to $0.0382$, so we can not reject the null hypothese. 


\textbf{Capture probability}

We can also see the simpler estimation of the capture probability that we know how to compute and compare with the approximation of it with our model (thanks to the formula (\ref{cum_subdensity})). And we get : 

```{r capture-prob, echo=FALSE, fig.align='center', fig.height=4, fig.width=7, warning=FALSE, message=FALSE}
#| fig-cap: Estimation and approximation of the probability of capture (with confidence interval). \label{capture_prob}

tar_load(fig_capture_prob)
fig_capture_prob
```


In the latter figure, we only displayed the trap where at least two captures happened. In orange, there is the value of the approximate probability using equation (\ref{cum_subdensity}) and in grey the empirical frequencies with the corresponding confidence interval. We can observe that the quantities of the model fit well with the non parametric estimation of the capture. 

\textbf{Log-Likelihood around the true parameters}

The log-likelihood given by equation (\ref{llh}) is tough to see because of the number of parameters we have. But we can try to display it, around the true parameters, and we get this figure : 


```{r llh, echo=FALSE, fig.align='center', fig.height=8, fig.width=12, warning=FALSE, message=FALSE}
#| fig-cap: Log-likelihood of the simulation according to all the parameters.  \label{fig-llh}
#tar_load(log_likelihood_sit_scenario)
#plot_loglik(log_likelihood_sit_scenario, lower_cutoff = -12e3)

tar_load(fig_loglik_sit_scenario)
fig_loglik_sit_scenario + theme_grey()
```

\newpage

The figure \ref{fig-llh} shows the surface of the partial likelihood for the parameters $D$ and $\lambda$ (the individuals parameters) for different values of $\alpha$ and $\beta$ (varying respectively by row an by column). The blue points are the true value of $D$ and $\lambda$ in the simulation (the figure for the true value of the capture parameters is displayed in the middle). 

We observe immediately a lack of identifiability for the capture parameters : indeed, all the figure of the diagonal seems to be the same, and it can be a problem if we want for example estimate the parameters by maximum likelihood.


# Conclusion

To conclude, we saw the model can be used to explain what happened after the release of the individuals. We observed a very good fitness for the conditional density and the capture probabilities. However, for the observation model, even with this good model, the likelihood does not give enough information about the full model. In deed, the lack of identifiability of the capture parameters, even if these ar not the principal motivation for the building of the model, can lead us to misestimate the individual parameters (i.e. diffusion and survival). 

There is the partial likelihood for for $D=150$ and $\lambda = 0.223$.

```{r crete,  echo=FALSE, fig.align='center', fig.height=4, fig.width=7, warning=FALSE, message=FALSE}
#| fig-cap: Partial log-likelihood of the simulation according to the captture parameters. \label{pllh}
tar_load(fig_crete)
fig_crete
```


\newpage

A first idea is to set a value to the $\beta$ parameter (or $\alpha$) and then compute the likelihood of the model with this fixed value. In the figure \ref{pllh} for instance, we cut horizontally or vertically (respectively for $\alpha$ or $\beta$) and the obtained subspace is better with less incertitude. But a new problem rises : which value of $\beta$ (or $\alpha$) should we choose ? Indeed, we finally just switch a parameter to a hyperparameter and choosing the parameter who maximizes the likelihood is the same as before...

Another idea in the same order of above, in that sense that we build a subspace of the valid area is that we can bind $\alpha$ and $\beta$ with a function (which respects the positive constraint). One way to do is that we can set a capture risk to one value (for a certain distance) and then deduce a relation between $\alpha$ and $\beta$. For example, assume that the capture risk (for constant location) is equal to $0.1$ for an individual located at 5 meters from the trap. Thus using the equation (\ref{hazard_unmoving}) we have : 

$$
\alpha e^{-25 \beta} = 0.1 \Longleftrightarrow \beta(\alpha) = \frac{\log(\alpha) - \log(0.1)}{25}
$$

\noindent \textbf{Note.} The valid values for alpha changed with this relation : we need to have $\alpha > 0.1$ to get $\beta(\alpha)>0$ too.



\newpage

# References



















