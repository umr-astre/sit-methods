#---------------------------- Initialization -----------------------------------
source("src/functions.R")
library(cubature)
library(dplyr)
library(furrr)
library(targets)

tar_load(sit_scenario)
tar_load(sit_scenario_1)
tar_load(sim_trap_agegroup)

D <- 150
alpha <- 0.8
params <- sit_scenario@parms
params1 <- sit_scenario_1@parms
beta <- (log(params$pc0) - log(params$pc5)) / 5**2
lambda <- -log(params$pds)

#------------------ New computations with overall survival approximation -------

# Model parameters
loc <- as.matrix(params$traps.loc[,c(2,3)])
pars <- list(pds = 0.8, diffusivity = 150, alpha = 0.8, beta = beta)

# Survival end specific hazard functions
S <- overall_survival(loc, pars)
h <- expected_specific_hazard_functions(loc, pars)

# Computation comparison with integrate, cubintegrate and alternative version of the integral
alter_S <- function(t){S(t/(1-t)) / (1 - t)**2}

bench::mark(base = integrate(S, 0, Inf, subdivisions = 1e5, rel.tol = 1e-2,stop.on.error = FALSE ), iterations = 10)
bench::mark(cubature = cubintegrate(S, 0, Inf, relTol =  1e-2), iterations = 10)
bench::mark(alternative = cubintegrate(alter_S, 0, 1, relTol =  1e-2), iterations = 10)

# observable llh with overall survival approximation
logLik_obs2 <- function(loc, pars) {
  hi <- expected_specific_hazard_functions(loc, pars)
  s <- function(t){exp(-log(pars$pds)*t)}

  function(i, ts, te, n) {
    ni <- length(i)  # number of observations
    stopifnot(
      length(ts) == ni,
      length(te) == ni
    )

    ans <- numeric(ni)

    ## Iterate over each observation j
    for(j in seq.int(ni)) {
      f <- function(t) {
        hit <- hi(t)
        hit$h[hit$i == i[j]] * s(t)
      }
      if(n[j]>0){
      ans[j] <- cubintegrate(
        f, ts[j], te[j], relTol = 1e-2
      )$integral
      }else{ans[j] <- 1}
    }
    return(log(ans))
  }
}

logLik_gen <- function(loc, dat, N) {

  ll <- function(l0, D, a, b) {

    stopifnot(l0 >= 0, D >= 0, a >= 0, b >= 0)
    pars <- list(
      pds = exp(-l0),
      diffusivity = D,
      alpha = a,
      beta = b
    )

    l0_f <- logLik_non_obs(loc, pars)  ## takes a few seconds
    li_f <- logLik_obs2(loc, pars)

    ## Vector of log-likelihoods for each single observation in the data
    lli <- li_f(dat[[1]], dat[[2]], dat[[3]], dat[[4]])  ## takes a few seconds

    ## Log-likelihood of the corresponding number of observations
    llis <- lli * dat[[4]]

    ## Total log-likelihood
    ans <- l0_f * (N - sum(dat[[4]])) + sum(llis)

    return(ans)
  }
  ## Vectorized version that exploits parallel computation
  llv <- function(l0, D, a, b) {
    future_pmap_dbl(
      .l = list(l0, D, a, b),
      .f = ll
    )
  }
}

#---------------------------- Optimisation of llh ------------------------------
# Load the data and parameters
tar_load(sim_trap_agegroup)
tar_load(sit_scenario)
params <- sit_scenario@parms
N <- params$ninds
loc <- params$traps.loc[ , c(2,3)] |> as.matrix()

# Data frame creation
levels(sim_trap_agegroup$age_group) <- c(1,2,3,4)
te <- c(0,5,10,15)
ts <- c(5,10,15,20)

dat <- sim_trap_agegroup |>
  mutate(ts = ts[as.numeric(age_group)]) |>
  mutate(te = te[as.numeric(age_group)]) |>
  relocate(id, te, ts, n) |>
  select(id, te, ts, n)


# Function optimization
llf <- logLik_gen(loc, dat, N)

obj <- function(param){
  return(-llf(l0 = exp(param[1]),
              D = exp(param[2]),
              a = exp(param[3]),
              b = exp(param[4])))
}

# Initial values
D <- params$diffusivity
lambda <- -log(params$pds)
alpha <- params$pc0
beta <- (log(params$pc0) - log(params$pc5)) / 5**2

# First estimation
init <- c(log(lambda), log(D), log(alpha), log(beta))
res <- optim(par = init, fn = obj, method = "BFGS", hessian = TRUE)

# Second estimation with others intial values
init2 <- c(log(0.7), log(200), log(5), log(1.5))
res2 <- optim(par = init2, fn = obj, method = "BFGS", hessian = TRUE)
# good results similar to the first one

# Third estimation with constraint instead of log scale
obj2 <- function(param){
  return(-llf(l0 = param[1],
              D = param[2],
              a = param[3],
              b = param[4]))
}

init3 <- c(lambda, 150, 0.8, beta)
res3 <- optim(par = init3, fn = obj2,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0,0,0,0))

init3 <- c(0.9, 200, 10, 1)
res <- optim(par = init3, fn = obj2,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0.01,0,0,0))

init3.2 <- c(0.1, 200, 1, 0.5)
res3.2 <- optim(par = init3.2, fn = obj2,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0,0,0,0))
# here we notice again the problem of the trap parameters seen in the graph

# Fourth estimation with link between alpha and beta

obj3 <- function(param){
  return(-llf(l0 = param[1],
              D = param[2],
              a = param[3],
              b = (log(param[3]) - log(0.1)) / 25))
}

init4 <- c(lambda, 150, 0.8)
res4 <- optim(par = init4, fn = obj3,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0, 0, 0.1))
# very good one, but constant capture frequency assumption

# Fifth estimation with another initial values
init5 <- c(0.1, 100, 0.8)
res5 <- optim(par = init5, fn = obj3,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0, 0, 0.1))

init6 <- c(0.1, 100, 0.7)
res6 <- optim(par = init6, fn = obj3,
              method = "L-BFGS-B",
              hessian = TRUE,lower = c(0, 0, 0.1))

lik_mat <- grid_around(
  c(D = D, alpha = alpha, beta = beta, lambda = lambda),
  p = .05,
  n = 21
)

res <- lik_mat |>
  filter(
    .data$D == .env$D,
    .data$lambda == .env$lambda
    ) |>
  rowwise() |>
  mutate(
    ll = llf(.data$lambda, .data$D, .data$alpha, .data$beta)
  )



p <- res |>
  ggplot() +
  aes(beta, alpha, fill = ll) +
  geom_raster() +
  geom_line(aes(x=beta, y= -8*beta + 1.45))+
  geom_point(aes(x= 0.083, y = 0.8), col = "red")+
  xlab(expression(beta))+
  ylab(expression(alpha))+
  scale_fill_viridis_c() +
  labs(fill = "Log-vraisemblance") +
  coord_cartesian(ylim = c(0.4,1.2))+
  theme_minimal()


a <- seq(0.4,1.2,0.01) |> as_tibble() |>
  mutate(z = (1.45-value) / 8) |>
  rowwise() |>
  mutate(f = llf(lambda, D, value, z))


a |> ggplot() + aes(x = value, y= f) + geom_line()


res[res$beta == 1.2 * beta, ] |> ggplot() + aes(x = alpha, ll) + geom_line()

ggsave("./../2023_acapel/src/Images/crete.png", p, height = 8, width = 11)


pars2 <- list(pds =0.77, diffusivity = 142.8959324, alpha = 12.4673133, beta = 1.0627619)

test <- gradient(loc, pars2)

# confidence area

H <- optimHess(res$par, fn = obj)

r <- 2.3

CA1 <- function(x1, x2){
  I <- solve(H / 50000)
  exp(res$par[1]) - (I[1,1] * x1 + I[1,2] * x2) / sqrt(50000)
}


CA2 <- function(x1, x2){
  I <- solve(H / 50000)
  exp(res$par[2]) - (I[2,1] * x1 + I[2,2] * x2) / sqrt(50000)
}


grille <- expand.grid(x,y) |>
  as_tibble() |>
  rowwise() |>
  mutate(z = CA1(Var1, Var2)) |>
  mutate(w = CA1(Var1, Var2)) |>
  mutate(indic = ifelse(Var1**2 + Var2**2 < r**2, 1, 0))


grille |> ggplot() + aes(x = z, y = w, col = indic) + geom_tile()

contour(indic ~ z + w, data = grille)




l <- seq(0.1,10, 0.1)

plot(l, llf(l, 200, 5, 2))

#--------------------------- Fisher scoring ---------------------------

# try to estimate the information fisher matrix using data and approximate
# the derivative by finite difference

gradient_obs <- function(loc, pars) {
  hi <- expected_specific_hazard_functions(loc, pars)
  s <- function(t){1-pexp(t, -log(pars$pds))}

  function(i, ts, te, n) {
    ni <- length(i)  # number of observations
    stopifnot(
      length(ts) == ni,
      length(te) == ni
    )

    ans <- matrix(numeric(4*ni), nc = 4)
    h <- 0.01
    ## Iterate over each observation j
    for(j in seq.int(ni)) {
      f <- function(t) {
        hit <- hi(t)
        hit$h[hit$i == i[j]] * s(t)
      }

      if(n[j]>0){
        int_value <- cubintegrate(
          f, ts[j], te[j], relTol = 1e-2
        )$integral
        for(k in seq.int(4)){
          g <- function(t){
            dh <- rep(0, 4)
            dh[k] <- h
            parsd <- as.list(as.numeric(pars) + dh)
            names(parsd) <- c("pds", "diffusivity", "alpha", "beta")
            hit <- expected_specific_hazard_functions(loc, parsd)(t)
            s1 <- function(t){1-pexp(t, -log(parsd$pds))}
            hit$h[hit$i == i[j]] * s1(t)

          }
          ans[j, k] <- cubintegrate(
            g, ts[j], te[j], relTol = 1e-2
          )$integral / (int_value * h) - 1 / h
        }
      }else{ans[j, ] <- rep(0, 4)}
    }
    return(ans)
  }
}

gradient_non_obs <- function(loc, pars) {

  s <- overall_survival(loc, pars, n_eval = 1e3)
  alter_s <- function(t){-log(pars$pds) * s(t/(1-t)) / (1 - t)**2}   # alternative version to integrate between 0 and 1
  int_s <- cubintegrate(
    alter_s, 0, 1, relTol = 1e-2
  )$integral
  ans <- numeric(4)
  h <- 0.01
  for(i in seq.int(4)){
    alter_ds <- function(t){
      dh <- rep(0, 4)
      dh[i] <- h
      parsd <- as.list(as.numeric(pars) + dh)
      names(parsd) <- c("pds", "diffusivity", "alpha", "beta")
      ds <- overall_survival(loc, pars, n_eval = 1e3)
      -log(parsd$pds) * ds(t/(1-t)) / (1 - t)**2}
    ans[i] <- cubintegrate(
      alter_ds, 0, 1, relTol = 1e-2
    )$integral / (int_s * h) - 1 / h
  }


  return(ans)
}

info_fisher <- function(loc, dat, N){
  fishermatrix <- function(l0, D, a, b){
    pars <- list(
      pds = exp(-l0),
      diffusivity = D,
      alpha = a,
      beta = b
    )

    g <- gradient_obs(loc, pars)
    g2 <- gradient_non_obs(loc, pars)


    ans <- matrix(rep(0, 4 * 4), nc = 4)
    grad <- g(dat[[1]], dat[[2]], dat[[3]], dat[[4]])
    for(i in 1: 84){
      ans <- ans + dat[[4]][i] * grad[i,] %*% t(grad[i,])
    }

    ans <- ans + (N - sum(dat[[4]])) * g2 %*% t(g2)
    return(
      list(
        info = ans / N,
        gradient = as.numeric(t(dat$n) %*% grad + (N - sum(dat[[4]])) * t(g2))
        )
      )
  }
}

I <- info_fisher(loc, dat, N)
res <- I(-log(params$pds), 150, 0.8, beta)

# seems to not working...





# approximation log prob
id <- rep(0, 21 * 4)
for(i in 0:20){
  id[4*i+(1:4)] <- rep(i+1,4)
}

library(cubature)

f<- logLik_obs(loc, pars)
res <- f(id, rep(c(0,5,10,15), 21), rep(c(5,10,15,20),21))


data <- list(theta = 1,
             y0 = 0,
             t0 = 0,
             ts = 1)

fit <- rstan::stan(file = "./test.stan", data = data)






