functions {
  /**
   * Return the value of the hazard function at time t, for parameters theta
   * at location x.
   *
   * @param t Positive number representing time.
   * @param theta Vector of parameters (of length 4) : 
   *     - theta[1] is the diffusion coefficient D.
   *     - theta[2] is the efficiency parameter alpha.
   *     - theta[3] is the scope parameter beta.
   *     - theta[4] is the survival parameter lambda.
   * @param x Vector of real value (length 2) which is the trap's location.
   */
  real hazard(real t,
              real[] theta,
              real[] x){
    // Initialisation of parameters
    real D = theta[1];
    real alpha = theta[2];
    real beta = theta[3];
    // Computation of some quantities
    real r = x[1]^2 + x[2]^2;
    real c = 1 / (1 + 4 * D * beta * t);
    // Result
    return alpha * exp(-beta * r * c) * c;
  }
  /**
   * Return the value of the cumumative hazard function at time t,
   * for parameters theta at location x. 
   *
   * By definition, it is the primitive function of the hazard function, so we 
   * use a numerical approximation to compute this quantity with the relation :
   *               F(x+h) \simeq F(x) + h*f(x),         for small h     (1)
   * as we know that F(0), we can compute everything easily by induction.
   *
   * @param t Positive number representing time.
   * @param theta Vector of parameters (of length 4) : 
   *     - theta[1] is the diffusion coefficient D.
   *     - theta[2] is the efficiency parameter alpha.
   *     - theta[3] is the scope parameter beta.
   *     - theta[4] is the survival parameter lambda.
   * @param x Vector of real value (length 2) which is the trap's location.
   * @param h Positive number, needed to compute the primitive approximation.
   */
  real cumulative_hazard(real t, 
                         real[] theta, 
                         real[] x, 
                         real h){
    real F = 0;
    int k = 0;
    // Leave the function immediatly if t = 0 (we know F(0)).
    if(t==0){
      return F;
    }
    // We divise [0,t] in interval [kh,(k+1)h] and compute F by induction using (1).
    while((k+1)*h<t){
      F += h * hazard(k * h, theta, x);
      k += 1;
    }
    // Final calculation of F using (1) at x = kh and h = t-kh.
    return F + (t - k * h) * hazard(k * h, theta, x);
  }
  /**
   * Return the value of the density function at time t, for parameters theta
   * at location x.
   *
   * For our model, the density is given by : 
   *        f(t) \propor h(t)*exp(-H(t))*exp(-lambda*t) 
   * where h is the hazard function of the model without death, and lambda the 
   * survival rate of the individuals.
   *
   * @param t Positive number representing time.
   * @param theta Vector of parameters (of length 4) : 
   *     - theta[1] is the diffusion coefficient D.
   *     - theta[2] is the efficiency parameter alpha.
   *     - theta[3] is the scope parameter beta.
   *     - theta[4] is the survival parameter lambda.
   * @param x Vector of real value (length 2) which is the trap's location.
   */
  real pre_density(real t,
                   real[] theta,
                   real[] x){
    // Initialization
    real lambda = theta[4];
    // Calculation of some quantities
    real h = hazard(t, theta, x);
    real H = cumulative_hazard(t, theta, x, 0.01);
    real survival = exp(-lambda * t);
    // Result
    return survival * h * exp(-H);
  }
}

data{
  int<lower=0> n;              // Number of periods
  int<lower=0> N;              // Number of individuals traped
  int<lower=0> K;              // Number of traps
  real t[n+1];                 // Period limits
  int<lower=1,upper=n> I[N];   // Period when individual is trapped
  int<lower=1,upper=K> C[N];   // Variable representing where one is trapped
  array[K, 2] real traps;      // Coordinates of the traps
}

parameters {
  real<lower=0> D;        // Diffusivity coefficient
  real<lower=0> alpha;    // Efficiency parameter
  real<lower=0> beta;     // Scope parameter
  real<lower=0> lambda;   // Survival parameter
}

model {
  D ~ beta(2, 2);
  alpha ~ beta(6, 1);
  beta ~ beta(1, 10);
  lambda ~ exponential(1);
  // Computation of the cumulative probability of capture of these parameters.
  array[K] vector[n] p;
  for(k in 1:K){
    for(i in 1:n){
      p[k, i] = (hazard(t[i+1], {D + 149.5, alpha, beta, lambda}, traps[k,])
                  +
                 hazard(t[i],{D + 149.5, alpha, beta, lambda}, traps[k,])) / 2 
                  *
                 (t[i + 1] - t[i]);
    }
    p[k, ] /= sum(p[k, ]);
  }
  // Computation of the log-likelihhod
  for(i in 1:N){
    target += log(p[C[i], I[i]]);
  }
}
