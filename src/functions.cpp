#include <Rcpp.h>
using namespace Rcpp;

/***R
source("./functions.R")
*/

void seed(int a){
  Function s = Environment::base_env()["set.seed"];
  s(a);
}

NumericVector sum_cpp(NumericVector v){
  Function sum = Environment::base_env()["sum"];
  return sum(v);
}

NumericMatrix brownian_cpp(float dt, int m_max, float D){
  Function b = Environment::global_env()["brownian"];
  return b(dt,m_max,D);
}

NumericVector lambda_x_cpp1(NumericVector x, NumericVector a, float alpha, float beta){
  Function l = Environment::global_env()["lambda_x"];
  return l(x,a, alpha, beta);
}

NumericVector lambda_x_cppI(NumericVector x, NumericMatrix A, float alpha, float beta){
  Function l = Environment::global_env()["lambda_x"];
  return l(x,A, alpha, beta);
}

NumericVector hhyperexp_cpp(NumericVector x, NumericVector lambda, NumericVector p){
  Function h = Environment::global_env()["hhyperexp"];
  return h(x, lambda, p);
}

// [[Rcpp::export]]
float sim1_cpp(float dt, NumericVector a, float D, int max, float alpha, float beta){
  NumericMatrix X = brownian_cpp(dt, max, D);
  int i = 0;
  NumericVector capt = {0};
  NumericVector l = 0;
  while(capt[0]==0&&i<max){
    l = lambda_x_cpp1(X.row(i),a,alpha,beta);
    i = i+1;
    capt = rbinom(1,1,l[0]*dt);
  }
  return (2*i-1)*dt/2;
}

// [[Rcpp::export]]
float capture_loop(NumericMatrix X, NumericMatrix A, float dt, float alpha, float beta){
  int i=0;
  int max = X.nrow();
  NumericVector capt={0};
  NumericVector l = 0;
  NumericVector prob = 0;
  while(capt[0]==0&&i<max){
    l = lambda_x_cppI(X.row(i),A,alpha,beta);
    prob = l/sum(l);
    capt = rbinom(1,1,dt*hhyperexp_cpp(dt,l,prob)[0]);
    i = i+1;
  }
  return i;
}
